package mx.unitec.moviles.practica4

import android.content.Context
import android.preference.PreferenceManager

private const val PRE_RFC = "dataRFC"


object DataPreferences {

    fun getStoredRFC(context: Context): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)

        return prefs.getString(PRE_RFC, "")!!

    }

    fun setStoredRFC(context: Context, query: String) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString(PRE_RFC, query)
            .apply()

    }
}